<h1>BBC News - Home</h1>
<?php
	$xml1 = new SimpleXMLElement("http://feeds.bbci.co.uk/news/business/rss.xml", NULL, TRUE);
	/*
	 * 2 Parts missing :
	 * - No link to the news on the title
	 * - No published date on each title
	 * 
	 * To see what is inside $xml1, comment out 'print_r($xml1)' below then view source code to see the inside $xml1 object
	 * 
	 * XPath query for searching is correct, but it's still a case-sensitive. It should be a case-insensitive
	 * 
	 */
	 
	 //print_r($xml1);
	foreach ($xml1->xpath('channel/item[contains(title, "' . $_GET['request'] . '")]') as $feed) {
		echo "    <h2>" . $feed->title . "</h2>\n";
		echo "    <p>" . $feed->description . "</p>\n";
	}
?>
<h1>CNN.com - Top Stories</h1>
<?php
	$xml2 = new SimpleXMLElement("http://rss.cnn.com/rss/edition.rss", NULL, TRUE);
	/*
	 * 2 Parts missing :
	 * - No link to the news on the title
	 * - No published date on each title
	 * 
	 * To see what is inside $xml2, comment out 'print_r($xml2)' below then view source code to see the inside $xml2 object
	 * 
	 * XPath query for searching is correct, but it's still a case-sensitive. It should be a case-insensitive
	 * 
	 */
	 
	 //print_r($xml2);
	foreach ($xml2->xpath('channel/item[contains(title, "' . $_GET['request'] . '")]') as $feed) {
		echo "    <h2>" . $feed->title . "</h2>\n";
		echo "    <p>" . $feed->description . "</p>\n";
	}
?>