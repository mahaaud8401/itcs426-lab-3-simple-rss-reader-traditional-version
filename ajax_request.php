<!--
	Adapting all of these into the given template.
-->
<script>
function sendSearch()
{
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	else
	  {// code for IE6, IE5
	  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    document.getElementById("show_feed").innerHTML=xmlhttp.responseText;
	    }
	  }
	var param_val = document.getElementById("search_form").value;
	xmlhttp.open("GET","ajax_response.php?request=" + param_val,true);
	xmlhttp.send();
}
</script>
<input type="text" id="search_form">
<input type="button" id="search_submit" value="Submit" onclick="sendSearch()" />
<div id="show_feed">
</div>
